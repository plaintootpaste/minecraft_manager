// Minecraft server manager.
use std::fs::remove_dir_all;
use std::path::PathBuf;
use std::process::Command;

use text_io::read;
use tinyfiledialogs as tfd;
use zip_extensions::*;

fn main() {
    loop {
        println!("Welcome to the minecraft server manager");
        println!("The following options are available (not case sensitive):");
        println!("    S - Start the server");
        println!("    L - Load a world");
        println!("    N - New world");
        println!("    B - backup the current world");
        println!("    Q - Quit");

        // read the user response
        let user_input: String = read!();
        let lower_case = user_input.to_lowercase();
        match lower_case.as_str() {
            "s" => start_server(),
            "l" => load_world(),
            "n" => new_world(),
            "b" => backup_world(),
            "q" => break,
            _ => println!("Your option {} is not suitable try again\n", user_input),
        }
    }
}
fn start_server() {
    //  start the minecraft server
    println!("Starting the server");

    let output = if cfg!(target_os = "windows") {
        Command::new("cmd")
            .args(&["/C", "java -Xmx2048M -Xms2048M -jar server.jar"])
            .output()
            .expect("failed to execute process")
    } else {
        Command::new("sh")
            .arg("-c")
            .arg("java -Xmx2048M -Xms2048M -jar server.jar")
            .output()
            .expect("failed to execute process")
    };
    println!("{:?}", output)
}
fn load_world() {
    //  start the minecraft server
    println!("Loading a custom zip world");
    let zip_path = tfd::open_file_dialog(
        "Open a zip file of a minecraft world",
        "",
        Some((&["*.mcb"], "Minecraft backup")),
    );
    let zip_path = match zip_path {
        Some(file) => {
            println!("The following backup world will be loaded:\n{}", file);
            let question =
                String::from("This will overwrite the current world, do you want to continue?");
            let should_do = verify(question);

            if should_do {
                println!("The world has been loaded");
            } else {
                println!("Going back to main menu");
                return;
            }
            file
        }
        None => {
            println!("No file selected, returning to main menu.");
            return;
        }
    };
    // clear the world file
    let world_path = PathBuf::from("world");
    if world_path.is_dir() {
        remove_dir_all(&world_path).unwrap();
    }
    std::fs::create_dir(&world_path).unwrap();

    //unzip the file and place in target path
    let zip_path = PathBuf::from(zip_path);
    zip_extract(&zip_path, &world_path).unwrap();
}
fn new_world() {
    //  start the minecraft server
    let question =
        String::from("Starting a new world, this will delete the current world, are you sure?");
    let should_do = verify(question);
    if should_do {
        println!("A new world will be generated");
    // generate a new world here
    } else {
        println!("Going back to main menu");
    }

    // clear the world file
    let target_path = PathBuf::from("world");
    if target_path.is_dir() {
        remove_dir_all(&target_path).unwrap();
    }
    std::fs::create_dir(&target_path).unwrap();
}
fn backup_world() {
    //  start the minecraft server
    println!("Backing up the current world to a zip.");
    let save_path = tfd::save_file_dialog_with_filter(
        "Save the current world to a .mcb (zip) file",
        "",
        &["*.mcb"],
        &"Mincraft backup file",
    );
    match save_path {
        Some(file_path) => {
            // a suitable path
            let mut file_path = PathBuf::from(file_path);
            file_path.set_extension("mcb");
            let world_path = PathBuf::from("world");
            zip_create_from_directory(&file_path, &world_path).unwrap();
        }
        None => {
            println!("No file path generated, returning to main menu.");
        }
    }
}
fn verify(question: String) -> bool {
    // simple function to check the user is sure
    println!("{}", question);
    println!("Y/N:");
    let user_input: String = read!();
    let lower_case = user_input.to_lowercase();
    match lower_case.as_str() {
        "y" => true,
        _ => false,
    }
}
